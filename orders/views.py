from builtins import print
# from pprint import pprint
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.views.generic import *
from django import forms
from .models import *


# Create your views here.

class GoodsCookiesMixin:
    def get_context_data(self, *, object_list=None, **kwards):
        # self.request.session.clear()
        context = super().get_context_data(object_list=None, **kwards)
        context['basket_items'] = len(self.request.session.get("basket", []))
        context['menu'] = True
        # pprint(context)
        print("basket:", self.request.session.get('basket', []))
        return context


class Index(GoodsCookiesMixin, ListView):
    model = Cathegory
    template_name = 'orders/index.html'


class AddToBasketForm(forms.Form):
    product = forms.CharField(max_length=100)

    def is_valid(self):
        return super().is_valid()


class AddToBasketModelForm(forms.Form):
    def __init__(self, cathegory, *args, **kwargs):
        super(AddToBasketModelForm, self).__init__(*args, **kwargs)
        self.fields['product'].queryset = Product.objects.all()

    class Meta:
        model = OderItems
        exclude = []


class AddGoodToBacket(FormView):
    form_class = AddToBasketModelForm
    # form_class = AddToBasketForm
    template_name = 'orders/product_form.html'
    success_url = "index"

    def form_valid(self, form):
        print(self.request.POST)
        return super().form_valid(form)


def add_to_basket(request, pk):
    cathegory = get_object_or_404(Cathegory, pk=pk)
    # print(request.session.keys())
    if request.method == "POST":
        # print(request.POST)
        form = dict(request.POST)
        # print(form, form.get('addon'))
        request.session['basket'] = request.session.get('basket', []) + \
                                    [[int(form['cathegory'][0]),
                                      int(form['product'][0]),
                                      [int(i) for i in form.get('addon', [])]
                                      ]]
        return redirect("index")
    return render(request, "orders/product_form.html", context={'cathegory': cathegory})


# @login_required
def show_basket(request):
    content = {}
    basket = []
    print(request.session.get('basket', []))
    for category, product, addon in request.session.get('basket', []):
        cathegory_name = Cathegory.objects.get(pk=category).name
        prod = Product.objects.get(pk=product)
        product_name = prod.name
        price = prod.price
        addons = [Addons.objects.get(pk=item).name for item in addon]
        basket.append({"cathegory": cathegory_name, "product": product_name, 'addons': addons, 'price': price})
    return render(request, "orders/basket.html", context={"basket": basket})


def remove_from_basket(request, pk):
    request.session.modified = True
    request.session['basket'].pop(pk)
    return redirect("basket")


def submit_order(request):
    print("I'm in the submit order ")
    print(request.method)
    if request.method == "POST":
        form = dict(request.POST)
        # print(form['tel'])
        if request.user.is_authenticated or form['tel']:
            if not len(request.session.get('basket', [])):
                print("Basket ampty")
                return redirect("index")
            # save order into database
            print(form)
            if request.user.is_authenticated:
                order = Order(user=User.objects.get(pk=request.user.id))
            else:
                order = Order(tel=form['tel'])
            order.save()
            print("Save order ")

            for category, product, adds in request.session['basket']:
                topping1 = None
                topping2 = None
                topping3 = None

                for n, add in enumerate(adds):
                    if n == 0:
                        topping1 = Addons.objects.get(pk=add)
                    if n == 1:
                        topping1 = Addons.objects.get(pk=add)
                    if n == 2:
                        topping2 = Addons.objects.get(pk=add)
                    order_item = OderItems(order=order,
                                           product=Product.objects.get(pk=product),
                                           large=1,
                                           toping1=topping1,
                                           toping2=topping2,
                                           toping3=topping3,
                                           price=Product.objects.get(pk=product).price)
                order_item.save()
                print("Save order - item ")
    del request.session['basket']
    request.session.modified = True

    return redirect("index")
