from django.contrib.auth.models import User
from django.db import models


class Cathegory(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True)

    def __str__(self):
        return f"{self.name}"


class Product(models.Model):
    cathegory = models.ForeignKey(Cathegory, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    large = models.BooleanField(default=True)
    price = models.FloatField()
    price1 = models.FloatField(null=True, blank=True)

    def __str__(self):
        return f"{self.cathegory.name}-{self.name}"


class Addons(models.Model):
    cathegory = models.ForeignKey(Cathegory, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.cathegory.name}- add-ons:{self.name}"


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, blank=True, null=True)
    data = models.DateTimeField(auto_now=True)
    done = models.BooleanField(default=False)
    checked = models.BooleanField(default=False)
    tel = models.CharField(max_length=15, blank=True, null=True, default=None)
    cost = models.FloatField(verbose_name="total sum", default=0)

    def __str__(self):
        return f"{self.tel or self.user.first_name}-{self.data.date()}"+\
               f"-{self.cost}$-  send-{self.done}"


class OderItems(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    large = models.BooleanField(default=False)
    toping1 = models.ForeignKey(Addons, on_delete=models.DO_NOTHING, default=None, null=True, blank=True,
                                related_name="first_topping")
    toping2 = models.ForeignKey(Addons, on_delete=models.DO_NOTHING, default=None, null=True, blank=True,
                                related_name="second_topping")
    toping3 = models.ForeignKey(Addons, on_delete=models.DO_NOTHING, default=None, null=True, blank=True,
                                related_name="therd_topping")
    price = models.FloatField()

    def __str__(self):
        return f"{self.product.name}-"