from django.urls import path

from . import views

urlpatterns = [
    # path("orders", views.AddGoodToBacket.as_view(), name="add_to_baskeet"),
    path("add_product/<int:pk>", views.add_to_basket, name="add_to_basket"),
    path('basket', views.show_basket, name="basket"),
    path("remove/<int:pk>", views.remove_from_basket, name="remove_form_basket"),
    path("submit", views.submit_order, name='submit'),
    path("", views.Index.as_view(), name="index")
]
